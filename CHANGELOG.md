1.1.0
=====

* Changed: Default status changed from 'published' to 'drafted'

1.0.1
=====

* Fixed: minor helper loading issue

1.0.0
=====

* Add support for more than just drafted/published states.
* Add configurable database column
* Make default state configurable
* Add helper + styles for `state_label`

0.2.0
=====

* Add Rails 4 support

0.0.2
=====

* Initial release
